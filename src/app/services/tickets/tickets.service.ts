import { Injectable } from '@angular/core';
import { TicketRestService } from "../ticket-rest/ticket-rest.service";
import { Observable, Subject, map } from "rxjs";
import {ICustomTourData, IGeneralTourData, INearestTour, ITour, ITourLocation, ITourType} from "../../models/tour";
import {IOrder} from "../../models/order";

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  private ticketSubject = new Subject<ITourType>()
  readonly ticketType$ = this.ticketSubject.asObservable();
  private ticketUpdateSubject = new Subject<ITour[]>();
  readonly ticketUpdateSubject$ = this.ticketUpdateSubject.asObservable();

  constructor(private ticketRestService: TicketRestService) { }

  getTickets(): Observable<ITour[]>{
    return this.ticketRestService.getTickets()
  }

  getTicketById(id: string): Observable<ITour>{
    return this.ticketRestService.getTicketById(id)
  }

  updateTour(type:ITourType): void {
    this.ticketSubject.next(type);
  }

  updateTicketList(data: ITour[]) {
    this.ticketUpdateSubject.next(data);
  }

  getError(){
    return this.ticketRestService.getRestError();
  }

  // getNearestTickets(): Observable<INearestTour[]>{
  //   return this.ticketRestService.getNearestTickets();
  // }
  //
  // getToursLocation(): Observable<ITourLocation[]>{
  //   return this.ticketRestService.getLocationList();
  // }
  //
  // transformData(data:INearestTour[], regions: ITourLocation[]): ICustomTourData[]{
  //   const newTourData: ICustomTourData[] = [];
  //   data.forEach((el) => {
  //     const newEl = <ICustomTourData>{...el};
  //     newEl.region = <ICustomTourData>regions.find((region) => region.id === el.locationId);
  //     newTourData.push(newEl);
  //   });
  //   return newTourData;
  // }
  //
  // getRandomNearestEvent(type: number): Observable<INearestTour>{
  //   return this.ticketRestService.getRandomNearestEvent(type)
  // }

  sendData(data: IOrder): Observable<any>{
    return this.ticketRestService.sendData(data);
  }

  createTour(data: any): Observable<any>{
    return this.ticketRestService.createTour(data)
  }

  getTicketsByName(name: string): Observable<ITour[]>{
    return this.ticketRestService.getTicketsByName(name)
  }
}
