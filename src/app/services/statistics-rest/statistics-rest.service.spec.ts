import { TestBed } from '@angular/core/testing';

import { StatisticsRestService } from './statistics-rest.service';

describe('StatisticsRestService', () => {
  let service: StatisticsRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StatisticsRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
