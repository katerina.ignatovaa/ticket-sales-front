import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IUserStatistics} from "../../models/user";

@Injectable({
  providedIn: 'root'
})
export class StatisticsRestService {

  constructor(private http: HttpClient) { }

  getUserStatistics(): Observable<IUserStatistics[]>{
    return this.http.get<IUserStatistics[]>('https://jsonplaceholder.typicode.com/users')
  }
}
