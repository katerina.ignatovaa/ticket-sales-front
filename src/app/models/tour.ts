export interface ITour{
  name: string,
  description: string,
  tourOperator: string,
  price: string,
  img: string,
  id: string,
  type: string,
  date: string,
  _id: string
}

export interface ITourType{
  label?: string,
  value?: string,
  date?: string
}

export interface INearestTour extends ITour{
  locationId: string
}

export interface ITourLocation{
  name: string,
  id: string
}

export interface ICustomTourData extends INearestTour{
  region: ITourLocation
}

export interface IGeneralTourData extends ITour{
  firstName: string,
  lastName: string,
  cardNmber: string,
  birthDai: Date,
  age: number
}
