import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api'
import { IUser } from "../../../models/user";
import {ConfigService} from "../../../services/config/config.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {IServerError} from "../../../models/error";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  login: string;
  email: string = '';
  password: string;
  passwordConfirm: string;
  cardNumber: string = '';
  isSelected: boolean;
  showCardNumber: boolean;

  constructor(private messageService: MessageService,
              private http: HttpClient) { }

  ngOnInit(): void {
    this.showCardNumber = ConfigService.config.useUserCard;
  }

  register(): void {
    const user: IUser = {
      login: this.login,
      email: this.email,
      password: this.password,
      cardNumber: this.cardNumber
    }
      if (this.password !== this.passwordConfirm) {
        this.messageService.add({severity: 'error', summary: "Пароли не совпадают"});
      } else {
        this.http.post<IUser>('http://localhost:3000/users/', user).subscribe((data) => {
            if (this.isSelected) {
              // localStorage.setItem('user_' + user.login, JSON.stringify(user));
              localStorage.setItem('user', JSON.stringify(user));
            }
            this.messageService.add({severity: 'success', summary: "Регистрация прошла успешно"});
          },
          (error: HttpErrorResponse) => {
            const serverError = <IServerError>error.error;
            this.messageService.add({severity: 'warning', summary: serverError.errorText});
          }
        )
    }
  }
}
