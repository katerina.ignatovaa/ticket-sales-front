import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { IUser } from "../../../models/user";
import { Router } from "@angular/router";
import { UserService } from "../../../services/user/user.service";
import {ConfigService} from "../../../services/config/config.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {IServerError} from "../../../models/error";

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})

export class AuthorizationComponent implements OnInit {

  login: string;
  password: string;
  isSelected: boolean;
  cardNumber: string = '';
  showCardNumber: boolean

  constructor(private userService: UserService,
              private messageService: MessageService,
              private router: Router,
              private http: HttpClient) {
  }

  ngOnInit(): void {
    this.showCardNumber = ConfigService.config.useUserCard;
  }

  logIn(): void {
    const user: IUser = {
      login: this.login,
      password: this.password,
      cardNumber: this.cardNumber
    }
      this.http.post<{access_token: string, id: string}>('http://localhost:3000/users/'+ user.login, user).subscribe((data) => {
        user.id = data.id;
        this.userService.setUser(user);
        const token: string = data.access_token;
        this.userService.setToken(token);
        this.router.navigate(['tickets/ticket-list'])
        },
      (error: HttpErrorResponse) => {
        const serverError = <IServerError>error.error;
        this.messageService.add({severity: 'error', summary: serverError.errorText});
      }
    );
  }
}
