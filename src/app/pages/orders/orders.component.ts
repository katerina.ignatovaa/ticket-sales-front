import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IOrder} from "../../models/order";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  orders: IOrder[];
  cols = [
    { field: 'tourId', header: 'Тур' },
    { field: 'userId', header: 'Пользователь' },
    { field: 'age', header: 'Возраст' },
    { field: 'birthDay', header: 'День рождения' },
    { field: 'cardNumber', header: 'Номер карты' }
  ];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<IOrder[]>('http://localhost:3000/orders').subscribe((data) => {
      data.forEach((el) => {
        el.birthDay = el.birthDay?.split('T')[0] || null;
      })
      this.orders = data;
    });
  }
}
