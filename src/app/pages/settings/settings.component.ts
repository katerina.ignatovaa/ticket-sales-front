import { Component, OnInit } from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import { SettingsService } from "../../services/settings/settings.service";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  subjectForUnsubscribe = new Subject()

  constructor(private settingsService: SettingsService) { }

  ngOnInit(): void {
   this.settingsService.loadSettings().pipe(takeUntil(this.subjectForUnsubscribe)).subscribe((data) => {
      console.log('observable ',data)
    });
    this.settingsService.getSettingsSubjectObservable().pipe(takeUntil(this.subjectForUnsubscribe)).subscribe((data) => {
      console.log('subject ',data)
    })
  }

  ngOnDestroy(): void{
    this.subjectForUnsubscribe.next('');
    this.subjectForUnsubscribe.complete();
  }

}
