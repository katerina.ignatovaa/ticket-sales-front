import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TicketsService} from "../../../services/tickets/tickets.service";

@Component({
  selector: 'app-add-tour',
  templateUrl: './add-tour.component.html',
  styleUrls: ['./add-tour.component.scss']
})
export class AddTourComponent implements OnInit {

  tourForm: FormGroup;

  constructor(private ticketsService: TicketsService) { }

  ngOnInit(): void {
    this.tourForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', [Validators.required, Validators.minLength(10)]),
      tourOperator: new FormControl(),
      price: new FormControl('', Validators.required),
      img: new FormControl(),
    })
  }

  createTour(){
    const tourData = this.tourForm.getRawValue();
    let formData = new FormData();
    for(let prop in tourData){
      formData.append(prop, tourData[prop])
    }
    this.ticketsService.createTour(formData).subscribe()
  }

  selectFile(ev: any): void{
    if(ev.currentFiles.length > 0){
      const file = ev.currentFiles[0];
      this.tourForm.patchValue({
        img: file
      });
    }
  }
}
