import { Component, OnInit } from '@angular/core';
import {StatisticsService} from "../../../services/statistics/statistics.service";
import {ICustomUserStatistics} from "../../../models/user";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  users: ICustomUserStatistics[];
  cols = [
    { field: 'name', header: 'Имя' },
    { field: 'company', header: 'Компания' },
    { field: 'phone', header: 'Телефон' },
    { field: 'city', header: 'Город' },
    { field: 'street', header: 'Улица' }
  ];

  constructor(private statisticsService: StatisticsService) { }

  ngOnInit(): void {
    this.statisticsService.getUserStatistics().subscribe((data) => {
      this.users = data;
    })
  }

}
