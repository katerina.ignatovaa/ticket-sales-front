import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  oldPassword:string;
  newPassword: string;
  newPasswordConfirm: string;

  constructor(private userService: UserService,
              private messageService: MessageService,) { }

  ngOnInit(): void {
  }

  changePassword(): void{
    if(this.oldPassword === this.userService.getUser().password){
      if(this.newPassword === this.newPasswordConfirm){
        const newUser = this.userService.getUser();
        newUser.password = this.newPassword;
        this.userService.setUser(newUser);
        this.messageService.add({severity:'success', summary:"Пароль успешно обновлен"});
      } else {
        this.messageService.add({severity: 'error', summary: "Подтверждение пароля на совпадает"});
      }
    } else {
      this.messageService.add({severity: 'error', summary: "Неправильно введен старый пароль"});
    }
  }

}
