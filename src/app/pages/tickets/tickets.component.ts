import {Component, OnDestroy, OnInit} from '@angular/core';
import { IMenuType } from "../../models/menuType";
import {UserService} from "../../services/user/user.service";

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit, OnDestroy {

  selectedType: IMenuType;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.userService.deleteUserInfo();
    window.localStorage.removeItem('index');
  }

  updateSelectedType(ev: IMenuType): void {
    this.selectedType = ev;
  }
}
