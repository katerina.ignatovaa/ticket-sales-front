import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { INearestTour, ITour, ITourLocation } from "../../../models/tour";
import { ActivatedRoute } from "@angular/router";
import { TiсketsStorageService } from "../../../services/tiсkets-storage/tiсkets-storage.service";
import { IUser } from "../../../models/user";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../../services/user/user.service";
import { TicketsService } from "../../../services/tickets/tickets.service";
import { forkJoin, fromEvent, Subscription } from "rxjs";
import {HttpClient} from "@angular/common/http";
import {IOrder} from "../../../models/order";

@Component({
  selector: 'app-ticket-item',
  templateUrl: './ticket-item.component.html',
  styleUrls: ['./ticket-item.component.scss']
})
export class TicketItemComponent implements OnInit, AfterViewInit, OnDestroy {

  ticket: ITour | undefined;
  user: IUser;
  userForm: FormGroup;
  @ViewChild('ticketSearch') ticketSearch: ElementRef;
  ticketSearchSub: Subscription;
  ticketRestSub: Subscription;
  ticketSearchValue: string;
  tours: ITour[];
  toursCopy: ITour[];

  constructor( private route: ActivatedRoute,
               private ticketStorage: TiсketsStorageService,
               private userService: UserService,
               private ticketsService: TicketsService,
               private http: HttpClient) { }

  ngOnInit(): void {
    this.user = this.userService.getUser();
    this.userForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', [Validators.required, Validators.minLength(2)]),
      cardNumber: new FormControl(),
      birthDay: new FormControl('', Validators.required),
      age: new FormControl('', [Validators.min(1)]),
      citizen: new FormControl()
    })

    const id = this.route.snapshot.queryParamMap.get('id');
    if(id){
      this.ticketsService.getTicketById(id).subscribe((data: ITour) => {
        this.ticket = data;
      })
    }
    this.ticketsService.getTickets().subscribe((data) => {
      this.tours = data;
      this.toursCopy = data;
    })
  }

  ngAfterViewInit() {
    this.userForm.controls["cardNumber"].setValue(this.user?.cardNumber);
    this.ticketSearchSub = fromEvent(this.ticketSearch.nativeElement, 'keyup').subscribe((ev) => {
        if(this.ticketSearchValue){
          this.ticketsService.getTicketsByName(this.ticketSearchValue).subscribe((data) => {
            this.tours = data;
          })
        } else {
          this.tours = [...this.toursCopy];
        }
    });
  }

  // initSearchTours(){
  //   const type = Math.floor(Math.random()* this.searchTypes.length);
  //   if(this.ticketRestSub && !this.ticketSearchSub.closed){
  //     this.ticketRestSub.unsubscribe();
  //   }
  //   this.ticketRestSub = this.ticketsService.getRandomNearestEvent(type).subscribe((data) => {
  //     this.nearestTours = this.ticketsService.transformData([data], this.toursLocation)
  //   })
  // }

  ngOnDestroy(){
    this.ticketSearchSub.unsubscribe();
  }

  sendTourData(): void{
    const userData = this.userForm.getRawValue();
    const generalData = {...this.ticket, ...userData};
    const userId = this.user?.id || null;
    const orderData: IOrder = {
      age: generalData.age,
      birthDay: generalData.birthDay,
      cardNumber: generalData.cardNumber,
      tourId: generalData._id,
      userId: userId
    }
    this.ticketsService.sendData(orderData).subscribe();
  }
}
